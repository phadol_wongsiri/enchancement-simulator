/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.enchancement_simulator_test;

/**
 *
 * @author Black Dragon
 */
public class Item {

    protected String name;
    protected char tier;

    public Item(String name, char tier) {
        this.name = name;
        this.tier = tier;
    }

    public String getName() {
        return name;
    }

    public char getTier() {
        return tier;
    }

    @Override
    public String toString() {
        System.out.println(name + "[" + tier + "]");
        return "";
    }
}
