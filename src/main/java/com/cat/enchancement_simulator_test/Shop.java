/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.enchancement_simulator_test;

/**
 *
 * @author Black Dragon
 */
public class Shop {

    private Item[] items = new Item[99];
    int itemCount = 0;
    private int[] pricelist = new int[99];

    public void addItemToShop(Item item, int price) {
        items[itemCount] = item;
        pricelist[itemCount] = price;
        itemCount++;
    }

    public void showShop() {
        System.out.println("---------------------------------");
        System.out.println("Shop");
        System.out.println("---------------------------------");
        for (int i = 0; i < itemCount; i++) {
            if (items[i] != null) {
                System.out.print((i + 1) + ".)");
                System.out.print(items[i]);
                System.out.println("Price :" + pricelist[i]);
            }
            System.out.println("---------------------------------");
        }
    }

    public void deleteItemFromShop(int z) {
        for (int i = z; i < itemCount - 1; i++) {
            items[i] = items[i + 1];
            pricelist[i] = pricelist[i + 1];
        }
        items[itemCount - 1] = null;
        itemCount--;
    }

    public Item buyItems(int n) {
        int i = n - 1;
        Item temp = new Item(items[i].getName(), items[i].getTier());
        if (items[i] instanceof Weapon) {
            Weapon wea = (Weapon) items[i];
            Weapon temp2 = new Weapon(wea.getName(), wea.getTier(), wea.gearScore, wea.growthRate, wea.enchancementLevel);
            return temp2;
        }
        if (items[i] instanceof Armor) {
            Armor arm = (Armor) items[i];
            Weapon temp2 = new Weapon(arm.getName(), arm.getTier(), arm.gearScore, arm.growthRate, arm.enchancementLevel);
            return temp2;
        }
        if (items[i] instanceof Accessory) {
            Accessory acc = (Accessory) items[i];
            Accessory temp2 = new Accessory(acc.getName(), acc.getTier(), acc.gearScore, acc.growthRate, acc.enchancementLevel);
            return temp2;
        }
        return temp;
    }

    public int getPricelist(int i) {
        return pricelist[i - 1];
    }

}
