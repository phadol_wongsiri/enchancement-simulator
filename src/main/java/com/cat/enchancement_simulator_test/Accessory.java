/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.enchancement_simulator_test;

/**
 *
 * @author Black Dragon
 */
public class Accessory extends Item {

    protected int gearScore;
    protected int growthRate;
    protected int enchancementLevel = 0;

    public Accessory(String name, char tier, int gearScore, int growthRate, int enchancementLevel) {
        super(name, tier);
        this.gearScore = gearScore;
        this.growthRate = growthRate;
        if (enchancementLevel <= 5) {
            this.enchancementLevel = enchancementLevel;
        } else {
            this.enchancementLevel = 0;
        }

    }

    public void setEnchancementLevel(int enchancementLevel) {
        if (enchancementLevel <= 5) {
            this.enchancementLevel = enchancementLevel;
        } else {
            System.out.println("The enchancement level must be inbetween 0 to 5.");
        }
    }

    public boolean notMaxEnchance() {
        return enchancementLevel != 5;
    }

    @Override
    public String toString() {
        System.out.print(name + " ");
        switch (enchancementLevel) {
            case 1:
                System.out.print("I");
                break;
            case 2:
                System.out.print("II");
                break;
            case 3:
                System.out.print("III");
                break;
            case 4:
                System.out.print("IV");
                break;
            case 5:
                System.out.print("V");
                break;
        }
        System.out.println();
        return "";
    }

    public int getGearScore() {
        return gearScore + (growthRate * enchancementLevel);
    }

    public double getEnchancementRate(int fs) {
        double finalRate = 0.00;
        switch (enchancementLevel) {
            case 0:
                finalRate = 25.00 + (2.50 * fs);
                break;
            case 1:
                finalRate = 10.00 + (1.00 * fs);
                break;
            case 2:
                finalRate = 7.50 + (0.75 * fs);
                break;
            case 3:
                finalRate = 2.50 + (0.25 * fs);
                break;
            case 4:
                finalRate = 0.50 + (0.05 * fs);
                break;
        }
        if (finalRate > 90.00) {
            finalRate = 90.00;
        }
        return finalRate;
    }

    public void increaseEnchancementLevel() {
        enchancementLevel++;
        if(enchancementLevel > 5){
            enchancementLevel = 5;
        }
    }

    public void increaseEnchancementLevel(int n) {
        for (int i = 0; i != n; i++) {
            increaseEnchancementLevel();
        }
    }

    public int getEnchancementLevel() {
        return enchancementLevel;
    }
}
