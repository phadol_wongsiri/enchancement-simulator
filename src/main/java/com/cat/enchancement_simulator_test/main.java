/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.enchancement_simulator_test;

import java.util.Scanner;
import java.lang.Math;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author Black Dragon
 */
public class main {

    static int money = 1000;
    static int currentFS = 0;
    static int FSslot1 = 100;
    static int FSslot2 = 70;
    static int FSslot3 = 50;
    static int FSslot4 = 20;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Inventory inventory = new Inventory();
        Shop shop = new Shop();
        shop.addItemToShop(new Item("Weapon black stone", 'C'), 150);
        shop.addItemToShop(new Item("Armor black stone", 'C'), 150);
        shop.addItemToShop(new Weapon("Weapon", 'S', 10, 5, 0), 250);
        shop.addItemToShop(new Armor("Armor", 'S', 10, 10, 0), 250);
        shop.addItemToShop(new Accessory("Ogre Ring", 'S', 10, 12, 0), 400);
        inventory.addAll(new Accessory("Ogre Ring", 'S', 10, 12, 1));
        inventory.addAll(new Accessory("Ogre Ring", 'S', 10, 12, 0));
        inventory.addAll(new Accessory("Ogre Ring", 'S', 10, 12, 0));
        inventory.addAll(new Weapon("Weapon", 'S', 10, 5, 0));
        while (true) {
            System.out.println("Money :" + money);
            inventory.showInventory();
            System.out.println("Type number to select item you want to enchance.");
            System.out.println("Type b to buy an item.");
            System.out.println("Type s to sell an item.");
            System.out.println("Type g to enter grind mode.");
            System.out.println("Type + to expand inventory slot.");
            System.out.println("Type q to exit.");
            String kb = sc.next();
            if (checkGmCommandLine(kb) != 0) {
                switch (checkGmCommandLine(kb)) {
                    case 1:
                        int s1 = sc.nextInt() - 1;
                        int n1 = sc.nextInt();
                        inventory.setCommand(s1, n1);
                        break;
                    case 2:
                        int n2 = sc.nextInt();
                        ;
                        money += n2;
                        break;
                    case 3:
                        int s3 = sc.nextInt() - 1;
                        int n3 = sc.nextInt();
                        inventory.upgradeCommand(s3, n3);
                        break;
                    case 4:
                        int n4 = sc.nextInt();
                        currentFS = n4;
                        break;
                }
            } else {
                char c = kb.charAt(0);
                if (checkInt(kb)) {
                    int num = Integer.parseInt(kb) - 1;
                    if (inventory.checkSelectedGear(num)) {
                        while (true) {
                            System.out.println("---------------------------------");
                            if (inventory.checkNotNull(num)) {
                                inventory.selectItemToEnchance(num, currentFS);
                            } else {
                                inventory.selectItemToEnchance(num - 1, currentFS);
                            }
                            System.out.println("---------------------------------");
                            System.out.println("FS slot1 :" + FSslot1);
                            System.out.println("FS slot2 :" + FSslot2);
                            System.out.println("FS slot3 :" + FSslot3);
                            System.out.println("FS slot4 :" + FSslot4);
                            System.out.println("Type number to swap failstack.");
                            System.out.println("Type + to enchance.");
                            System.out.println("Type q to go back.");
                            String kb2 = sc.next();
                            if (checkGmCommandLine(kb2) != 0) {
                                switch (checkGmCommandLine(kb2)) {
                                    case 1:
                                        int s1 = sc.nextInt() - 1;
                                        int n1 = sc.nextInt();
                                        inventory.setCommand(s1, n1);
                                        break;
                                    case 2:
                                        int n2 = sc.nextInt();
                                        ;
                                        money += n2;
                                        break;
                                    case 3:
                                        int s3 = sc.nextInt() - 1;
                                        int n3 = sc.nextInt();
                                        inventory.upgradeCommand(s3, n3);
                                        break;
                                    case 4:
                                        int n4 = sc.nextInt();
                                        currentFS = n4;
                                        break;
                                }
                            } else {
                                if (checkInt(kb2)) {
                                    int num2 = Integer.parseInt(kb2);
                                    if (num2 >= 1 && num2 <= 4) {
                                        fsSwap(num2);
                                    } else {
                                        System.out.println("Failstack save have 4 slot.");
                                    }
                                }
                                char c2 = kb2.charAt(0);
                                if (c2 == '+') {
                                    double randNum = getRandomNum();
                                    switch (inventory.enchanceSelectedItem(num, currentFS, randNum)) {
                                        case 'w':
                                            System.out.println("You don't have Weapon black stone.");
                                            break;
                                        case 'r':
                                            System.out.println("You don't have Armor black stone.");
                                            break;
                                        case 't':
                                            currentFS = 0;
                                            break;
                                        case 'a':
                                            currentFS++;
                                            c2 = 'q';
                                            break;
                                        case 'e':
                                            break;
                                        case 'f':
                                            currentFS++;
                                            break;
                                    }
                                }
                                if (c2 == 'q') {
                                    break;
                                }
                            }
                        }
                    } else {
                        System.out.println("Item can not be enchance.");
                    }
                }
                if (c == 'b') {
                    while (true) {
                        shop.showShop();
                        System.out.println("Money :" + money);
                        System.out.println("Select item you want to buy.");
                        System.out.println("Type q to go back");
                        String strc2 = sc.next();
                        if (checkGmCommandLine(strc2) != 0) {
                            switch (checkGmCommandLine(strc2)) {
                                case 1:
                                    int s1 = sc.nextInt() - 1;
                                    int n1 = sc.nextInt();
                                    inventory.setCommand(s1, n1);
                                    break;
                                case 2:
                                    int n2 = sc.nextInt();
                                    ;
                                    money += n2;
                                    break;
                                case 3:
                                    int s3 = sc.nextInt() - 1;
                                    int n3 = sc.nextInt();
                                    inventory.upgradeCommand(s3, n3);
                                    break;
                                case 4:
                                    int n4 = sc.nextInt();
                                    currentFS = n4;
                                    break;
                            }
                        } else {
                            if (inventory.canAddItem()) {
                                if (checkInt(strc2)) {
                                    int num = Integer.parseInt(strc2);
                                    System.out.println("How much do you want to buy?");
                                    String st = sc.next();
                                    if (checkInt(st)) {
                                        int t = Integer.parseInt(st);
                                        if (num <= shop.itemCount) {
                                            if (shop.getPricelist(num) * t <= money) {
                                                for (int i = 0; i < t; i++) {
                                                    if (inventory.canAddItem()) {
                                                        money -= shop.getPricelist(num);
                                                        inventory.addAll(shop.buyItems(num));
                                                    } else {
                                                        System.out.println("Your inventory is full!");
                                                        break;
                                                    }
                                                }
                                                makingSpace();
                                                System.out.println("You brought an item.");
                                            } else {
                                                makingSpace();
                                                System.out.println("You don't have enough money.");
                                            }
                                        }
                                    }
                                }
                            } else {
                                makingSpace();
                                System.out.println("Your inventory is full!");
                            }
                            char c2 = strc2.charAt(0);
                            if (c2 == 'q') {
                                break;
                            }
                        }
                    }
                }
                if (c == 's') {
                    if (inventory.gearCount != 0) {
                        while (true) {
                            inventory.showInventoryWithPrice();
                            System.out.println("Money :" + money);
                            System.out.println("Select item you want to sell.");
                            System.out.println("Type q to go back");
                            String kb2 = sc.next();
                            if (checkGmCommandLine(kb2) != 0) {
                                switch (checkGmCommandLine(kb2)) {
                                    case 1:
                                        int s1 = sc.nextInt() - 1;
                                        int n1 = sc.nextInt();
                                        inventory.setCommand(s1, n1);
                                        break;
                                    case 2:
                                        int n2 = sc.nextInt();
                                        ;
                                        money += n2;
                                        break;
                                    case 3:
                                        int s3 = sc.nextInt() - 1;
                                        int n3 = sc.nextInt();
                                        inventory.upgradeCommand(s3, n3);
                                        break;
                                    case 4:
                                        int n4 = sc.nextInt();
                                        currentFS = n4;
                                        break;
                                }
                            } else {
                                if (checkInt(kb2)) {
                                    int num = Integer.parseInt(kb2) - 1;
                                    money += inventory.priceTable(num);
                                    inventory.deleteGear(num);
                                    System.out.println("Item sold.");
                                }
                                char c2 = kb2.charAt(0);
                                if (c2 == 'q') {
                                    break;
                                }
                            }
                        }
                    } else {
                        System.out.println("You don't have item that can be sell.");
                    }
                }
                if (c == 'g') {
                    System.out.println("Press q to go back.");
                    while (true) {
                        int n = ((int) getRandomNum());
                        if (n >= 32 && n <= 126) {
                            System.out.println("Money :" + money);
                            char rchar = (char) n;
                            System.out.println("to grind press :" + rchar);
                            char ct = sc.next().charAt(0);
                            if (ct == 'q') {
                                break;
                            }
                            if (ct == rchar) {
                                grind();
                            }
                        }
                    }
                }
                if (c == '+') {
                    System.out.println("How many slot you want to increase?");
                    System.out.println("1 slot > cost > 1000 money.");
                    String kb2 = sc.next();
                    if (checkInt(kb2)) {
                        int num = Integer.parseInt(kb2);
                        if (num * 1000 <= money) {
                            money -= num * 1000;
                            inventory.increaseSlot(num);
                        }
                    }
                }
                if (c == 'q') {
                    System.out.println("Are you sure? (press q again to exit.)");
                    char confirmExit = sc.next().charAt(0);
                    if (confirmExit == 'q') {
                        break;
                    }
                }
            }
        }
    }

    public static boolean checkInt(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            int d = Integer.parseInt(strNum);
        } catch (NumberFormatException d) {
            return false;
        }
        return true;
    }

    //overload
    public static boolean checkInt(char strNum) {
        try {
            int d = Character.getNumericValue(strNum);
        } catch (NumberFormatException d) {
            return false;
        }
        return true;
    }

    public static double getRandomNum() {
        double rand = Math.random() * 100;
        BigDecimal bd = new BigDecimal(rand).setScale(2, RoundingMode.HALF_UP);
        double finalrand = bd.doubleValue();
        return finalrand;
    }

    public static void makingSpace() {
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
    }

    public static void makingSpace(int n) {
        for (int i = 0; i < n; i++) {
            System.out.println();
        }
    }

    public static void grind() {
        int num = (int) getRandomNum();
        money += num;
    }

    public static void grind(int n) {
        for (int i = 0; i < n; i++) {
            int num = (int) getRandomNum();
            money += num;
        }
    }

    public static void fsSwap(int num) {
        int temp;
        switch (num) {
            case 1:
                temp = currentFS;
                currentFS = FSslot1;
                FSslot1 = temp;
                break;
            case 2:
                temp = currentFS;
                currentFS = FSslot2;
                FSslot2 = temp;
                break;
            case 3:
                temp = currentFS;
                currentFS = FSslot3;
                FSslot3 = temp;
                break;
            case 4:
                temp = currentFS;
                currentFS = FSslot4;
                FSslot4 = temp;
                break;

        }
    }

    public static int checkGmCommandLine(String kb) {
        if (kb.startsWith("/setfs")) {
            return 4;
        }
        if (kb.startsWith("/set")) {

            return 1;
        }
        if (kb.startsWith("/add")) {
            return 2;
        }
        if (kb.startsWith("/upgrade")) {
            return 3;
        }
        return 0;
    }
}
