/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.enchancement_simulator_test;

/**
 *
 * @author Black Dragon
 */
public class Armor extends Equipment {

    public Armor(String name, char tier, int gearScore, int growthRate, int enchancementLevel) {
        super(name, tier, gearScore, growthRate, enchancementLevel);
    }

    public double getEnchancementRate(int fs) {
        double finalRate = 0.00;
        switch (enchancementLevel) {
            case 5:
                finalRate = 70.00 + (1.40 * fs);
                break;
            case 6:
                finalRate = 25.64 + (2.56 * fs);
                break;
            case 7:
                finalRate = 17.24 + (1.72 * fs);
                break;
            case 8:
                finalRate = 11.76 + (1.18 * fs);
                break;
            case 9:
                finalRate = 7.69 + (0.77 * fs);
                break;
            case 10:
                finalRate = 6.25 + (0.63 * fs);
                break;
            case 11:
                finalRate = 5.00 + (0.50 * fs);
                break;
            case 12:
                finalRate = 4.00 + (0.40 * fs);
                break;
            case 13:
                finalRate = 2.86 + (0.29 * fs);
                break;
            case 14:
                finalRate = 2.00 + (0.20 * fs);
                break;
            case 15:
                finalRate = 11.76 + (1.18 * fs);
                break;
            case 16:
                finalRate = 7.69 + (0.77 * fs);
                break;
            case 17:
                finalRate = 6.25 + (0.63 * fs);
                break;
            case 18:
                finalRate = 2.00 + (0.20 * fs);
                break;
            case 19:
                finalRate = 0.30 + (0.03 * fs);
                break;
        }
        if (finalRate > 90.00) {
            finalRate = 90.00;
        }
        if (enchancementLevel <= 4) {
            finalRate = 100.00;
        }
        return finalRate;
    }
}
