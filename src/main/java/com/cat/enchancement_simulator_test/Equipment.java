/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.enchancement_simulator_test;

/**
 *
 * @author Black Dragon
 */
public class Equipment extends Item {

    protected int gearScore;
    protected int growthRate;
    protected int enchancementLevel = 0;

    public Equipment(String name, char tier, int gearScore, int growthRate, int enchancementLevel) {
        super(name, tier);
        this.gearScore = gearScore;
        this.growthRate = growthRate;
        if (enchancementLevel <= 20) {
            this.enchancementLevel = enchancementLevel;
        } else {
            this.enchancementLevel = 0;
        }
    }

    public void setEnchancementLevel(int enchancementLevel) {
        if (enchancementLevel <= 20) {
            this.enchancementLevel = enchancementLevel;
        } else {
            System.out.println("The enchancement level must be inbetween 0 to 20.");
        }
    }

    public boolean notMaxEnchanced() {
        return enchancementLevel != 20;
    }

    @Override
    public String toString() {
        System.out.print(name + " ");
        if (enchancementLevel <= 15) {
            System.out.print("+" + enchancementLevel);
        } else {
            switch (enchancementLevel) {
                case 16:
                    System.out.print("I");
                    break;
                case 17:
                    System.out.print("II");
                    break;
                case 18:
                    System.out.print("III");
                    break;
                case 19:
                    System.out.print("IV");
                    break;
                case 20:
                    System.out.print("V");
                    break;
            }
        }
        System.out.println();
        return "";
    }

    public int getGearScore() {
        return gearScore + (growthRate * enchancementLevel);
    }

    public void increaseEnchancementLevel() {
        enchancementLevel++;
        if(enchancementLevel > 20){
            enchancementLevel = 20;
        }
    }

    public void increaseEnchancementLevel(int n) {
        for (int i = 0; i != n; i++) {
            increaseEnchancementLevel();
        }
    }

    public void decreaseEnchancementLevel() {
        enchancementLevel--;
    }

    public void decreaseEnchancementLevel(int n) {
        for (int i = 0; i != n; i++) {
            enchancementLevel--;
        }
    }

    public int getEnchancementLevel() {
        return enchancementLevel;
    }
}
