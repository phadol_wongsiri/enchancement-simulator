/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.enchancement_simulator_test;

/**
 *
 * @author Black Dragon
 */
public class Inventory {

    private int slot = 20;

    private Item[] items = new Item[slot];
    private Item[] gears = new Item[slot];
    int itemCount = 0;
    int gearCount = 0;

    public void increaseSlot(int num) {
        slot += num;
    }

    public boolean canAddItem() {
        return itemCount + gearCount < slot;
    }

    public void showInventory() {
        System.out.println("Inventory slot " + (gearCount + itemCount) + "/" + slot);
        System.out.println("---------------------------------");
        for (int i = 0; i < gearCount; i++) {
            if (gears[i] != null) {
                System.out.print((i + 1) + ".)");
                System.out.println(gears[i]);
            }
        }
        for (int i = 0; i < itemCount; i++) {
            if (items[i] != null) {
                System.out.print(((i + gearCount) + 1) + ".)");
                System.out.println(items[i]);
            }
        }
        System.out.println("---------------------------------");
    }

    public void addAll(Item item) {
        if (item instanceof Weapon || item instanceof Armor || item instanceof Accessory) {
            addGear(item);
        } else {
            addItem(item);
        }
    }

    public void addItem(Item item) {
        if (canAddItem()) {
            items[itemCount] = item;
            itemCount++;
        } else {
            System.out.println("Cannot add an item.");
        }
    }

    public void addGear(Item item) {
        if (canAddItem()) {
            gears[gearCount] = item;
            gearCount++;
        } else {
            System.out.println("Cannot add an item.");
        }
    }

    public void selectItemToEnchance(int i, int fs) {
        System.out.println("Selected item:");
        System.out.println(gears[i]);
        if (gears[i] instanceof Equipment) {
            Equipment equip = (Equipment) gears[i];
            System.out.println("Gear Score:" + equip.getGearScore());
            System.out.println("Current Fail Stack: " + fs);
            if (gears[i] instanceof Weapon) {
                Weapon weapon = (Weapon) gears[i];
                System.out.printf("Enchance Rate: %.2f", weapon.getEnchancementRate(fs));
                System.out.println("%");
            } else if (gears[i] instanceof Armor) {
                Armor armor = (Armor) gears[i];
                System.out.printf("Enchance Rate: %.2f", armor.getEnchancementRate(fs));
                System.out.println("%");
            }
        } else if (gears[i] instanceof Accessory) {
            Accessory acc = (Accessory) gears[i];
            System.out.println("Gear Score:" + acc.getGearScore());
            System.out.println("Current Fail Stack: " + fs);
            System.out.printf("Enchance Rate: %.2f", acc.getEnchancementRate(fs));
            System.out.println("%");
        }
    }

    public boolean checkSelectedGear(int i) {
        return gears[i] instanceof Weapon || gears[i] instanceof Armor || gears[i] instanceof Accessory;
    }

    public char enchanceSelectedItem(int i, int fs, double chance) {
        if (gears[i] instanceof Weapon) {
            Weapon weapon = (Weapon) gears[i];
            if (deleteWeaponBlackStone()) {
                if (chance <= weapon.getEnchancementRate(fs)) {
                    System.out.println("---------------------------------");
                    System.out.println("Enchancement Successful.");
                    weapon.increaseEnchancementLevel();
                    if (weapon.getEnchancementLevel() <= 7) {
                        return 'e';
                    }
                    return 't';
                } else {
                    System.out.println("---------------------------------");
                    System.out.println("Enchancement failed.");
                    if (weapon.getEnchancementLevel() > 16) {
                        weapon.decreaseEnchancementLevel();
                    }
                    return 'f';
                }
            } else {
                return 'w';
            }
        } else if (gears[i] instanceof Armor) {
            Armor armor = (Armor) gears[i];
            if (deleteArmorBlackStone()) {
                if (chance <= armor.getEnchancementRate(fs)) {
                    System.out.println("---------------------------------");
                    System.out.println("Enchancement Successful.");
                    armor.increaseEnchancementLevel();
                    if (armor.getEnchancementLevel() <= 5) {
                        return 'e';
                    }
                    return 't';
                } else {
                    System.out.println("---------------------------------");
                    System.out.println("Enchancement failed.");
                    if (armor.getEnchancementLevel() > 16) {
                        armor.decreaseEnchancementLevel();
                    }
                    return 'f';
                }
            } else {
                return 'r';
            }
        } else if (gears[i] instanceof Accessory) {
            Accessory acc = (Accessory) gears[i];
            if (deleteAcc(i)) {
                if (chance <= acc.getEnchancementRate(fs)) {
                    System.out.println("---------------------------------");
                    System.out.println("Enchancement Successful.");
                    acc.increaseEnchancementLevel();
                    return 't';
                } else {
                    System.out.println("---------------------------------");
                    System.out.println("Enchancement failed.");
                    deleteGear(i);
                    return 'a';
                }
            } else {
                System.out.println("You need another accessory with same name.");
            }
        }
        return 'd';
    }

    public void deleteItem(int z) {
        for (int i = z; i < itemCount - 1; i++) {
            items[i] = items[i + 1];
        }
        items[itemCount - 1] = null;
        itemCount--;
    }

    public void deleteGear(int z) {
        for (int i = z; i < gearCount - 1; i++) {
            gears[i] = gears[i + 1];
        }
        gears[gearCount - 1] = null;
        gearCount--;
    }

    public boolean deleteAcc(int z) {
        int c = 99;
        for (int i = 0; i < gearCount; i++) {
            if (i != z) {
                if (gears[i] instanceof Accessory) {
                    Accessory acc = (Accessory) gears[i];
                    if (acc.getEnchancementLevel() == 0) {
                        c = i;
                        break;
                    }
                }
            }
        }
        if (c != 99) {
            for (int i = c; i < gearCount - 1; i++) {
                gears[i] = gears[i + 1];
            }
            gears[gearCount - 1] = null;
            gearCount--;
            return true;
        } else {
            return false;
        }
    }

    public void showInventoryWithPrice() {
        System.out.println("Inventory slot " + itemCount + "/" + slot);
        System.out.println("---------------------------------");
        for (int i = 0; i < gearCount; i++) {
            if (gears[i] != null) {
                System.out.print((i + 1) + ".)");
                System.out.print(gears[i]);
                int price = 0;
                price = priceTable(i);
                System.out.println("Price :" + price);
            }
            System.out.println("---------------------------------");
        }
    }

    public int priceTable(int i) {
        int price = 0;
        if (gears[i] instanceof Weapon) {
            Weapon weapon = (Weapon) gears[i];
            if (weapon.getEnchancementLevel() <= 7) {
                price = 250 + (weapon.getEnchancementLevel() * 150);
            } else if (weapon.getEnchancementLevel() <= 15) {
                price = 1300;
                for (int l = weapon.getEnchancementLevel() - 7; l != 0; l--) {
                    price += 150 + (l * 50);
                }
            } else {
                switch (weapon.getEnchancementLevel()) {
                    case 16:
                        price = 5000;
                        break;
                    case 17:
                        price = 7500;
                        break;
                    case 18:
                        price = 16800;
                        break;
                    case 19:
                        price = 42100;
                        break;
                    case 20:
                        price = 208200;
                        break;
                }
            }
        } else if (gears[i] instanceof Armor) {
            Armor armor = (Armor) gears[i];
            if (armor.getEnchancementLevel() <= 4) {
                price = 250 + (armor.getEnchancementLevel() * 150);
            } else if (armor.getEnchancementLevel() <= 15) {
                price = 1300;
                for (int l = armor.getEnchancementLevel() - 4; l != 0; l--) {
                    price += 150 + (l * 50);
                }
            } else {
                switch (armor.getEnchancementLevel()) {
                    case 16:
                        price = 6000;
                        break;
                    case 17:
                        price = 9000;
                        break;
                    case 18:
                        price = 23700;
                        break;
                    case 19:
                        price = 56300;
                        break;
                    case 20:
                        price = 237200;
                        break;
                }
            }
        } else if (gears[i] instanceof Accessory) {
            Accessory acc = (Accessory) gears[i];
            switch (acc.getEnchancementLevel()) {
                case 0:
                    price = 400;
                    break;
                case 1:
                    price = 1000;
                    break;
                case 2:
                    price = 3500;
                    break;
                case 3:
                    price = 15800;
                    break;
                case 4:
                    price = 86600;
                    break;
                case 5:
                    price = 563000;
                    break;
            }
        }
        return price;
    }

    public boolean deleteWeaponBlackStone() {
        for (int i = 0; i < itemCount; i++) {
            if (items[i].getName().equals("Weapon black stone")) {
                deleteItem(i);
                return true;
            }
        }
        return false;
    }

    public boolean deleteArmorBlackStone() {
        for (int i = 0; i < itemCount; i++) {
            if (items[i].getName().equals("Armor black stone")) {
                deleteItem(i);
                return true;
            }
        }
        return false;
    }

    public boolean checkNotNull(int i) {
        if (gears[i] != null) {
            return true;
        }
        return false;
    }

    public void setCommand(int s, int i) {
        if (gears[s] instanceof Equipment) {
            Equipment equ = (Equipment) gears[s];
            equ.setEnchancementLevel(i);
        }
        if (gears[s] instanceof Accessory) {
            Accessory acc = (Accessory) gears[s];
            acc.setEnchancementLevel(i);
        }
    }

    public void upgradeCommand(int s, int i) {
        if (gears[s] instanceof Equipment) {
            Equipment equ = (Equipment) gears[s];
            equ.increaseEnchancementLevel(i);
        }
        if (gears[s] instanceof Accessory) {
            Accessory acc = (Accessory) gears[s];
            acc.increaseEnchancementLevel(i);
        }
    }
}
